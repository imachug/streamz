export const TIMESLICE = 200;
export const PING_INTERVAL = 60000;
export const PACKET_DELAY = 10000;