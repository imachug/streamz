import "@/libs/polyfill";

import "./sass/main.sass";

import Vue from "vue/dist/vue.min.js";

import AsyncComputed from "vue-async-computed";
Vue.use(AsyncComputed);

// Components

import Icon from "vue-awesome/components/Icon.vue";
Vue.component("icon", Icon);

import Header from "@/vue_components/Header/Header.vue";
Vue.component("Header", Header);

import StreamZ from "@/vue_components/StreamZ.vue";
Vue.component("StreamZ", StreamZ);

import ClearFix from "@/vue_components/ClearFix.vue";
Vue.component("ClearFix", ClearFix);

import VideoPlayer from "@/vue_components/VideoPlayer/VideoPlayer.vue";
Vue.component("VideoPlayer", VideoPlayer);

import Comments from "@/vue_components/Comments/Comments.vue";
Vue.component("Comments", Comments);

import ExtendedInput from "@/vue_components/ExtendedInput.vue";
Vue.component("ExtendedInput", ExtendedInput);

Vue.prototype.$eventBus = new Vue();

import Vuex from "vuex";
Vue.use(Vuex);
const store = new Vuex.Store({
	state: {
		siteInfo: {
			settings: {
				own: false,
				permissions: []
			}
		},
		currentParams: null,
		currentRoute: null,
		currentHash: null
	},
	mutations: {
		setSiteInfo(state, siteInfo) {
			state.siteInfo = siteInfo;
		},
		route(state, router) {
			state.currentParams = router.currentParams;
			state.currentRoute = router.currentRoute;
			state.currentHash = router.currentHash;
		}
	}
});

import root from "./vue_components/root.vue";
var app = new Vue({
	el: "#app",
	render: h => h(root),
	data: {
		currentView: null
	},
	store
});

import {route} from "./route.js";
import {zeroPage} from "./zero";

Vue.prototype.$zeroPage = zeroPage;

(async function() {
	const siteInfo = await zeroPage.getSiteInfo();
	store.commit("setSiteInfo", siteInfo);
	route(app);
	app.$eventBus.$emit("setSiteInfo", siteInfo);
})();
zeroPage.on("setSiteInfo", msg => {
	if(msg.params.address === store.state.siteInfo.address) {
		// From StreamZ, not from a hub
		store.commit("setSiteInfo", msg.params);
		app.$eventBus.$emit("setSiteInfo", msg.params);
	}
});


import {handlePeerReceive} from "@/libs/peer.js";
zeroPage.on("peerReceive", ({params}) => {
	handlePeerReceive(params);
});