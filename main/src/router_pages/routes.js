import Home from "./home/home.vue";
import About from "./about/about.vue";
import Search from "./search/search.vue";
import Stream from "./stream/stream.vue";
import StreamConfiguration from "./stream/configuration.vue";
import Watch from "./watch/watch.vue";

export default vue => [
	{
		path: "",
		controller: () => {
			vue.currentView = Home;
		}
	},
	{
		path: "about",
		controller: () => {
			vue.currentView = About;
		}
	},
	{
		path: "search",
		controller: () => {
			vue.currentView = Search;
		}
	},
	{
		path: "search/:query",
		controller: () => {
			vue.currentView = Search;
		}
	},
	{
		path: "stream",
		controller: () => {
			vue.currentView = StreamConfiguration;
		}
	},
	{
		path: "stream/:title/:description/:delay/:screen/:camera",
		controller: () => {
			vue.currentView = Stream;
		}
	},
	{
		path: "watch/:hub/:id/:title",
		controller: () => {
			vue.currentView = Watch;
		}
	}
];