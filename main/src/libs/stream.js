import EventEmitter from "wolfy87-eventemitter";
import {Buffer} from "buffer";

export default class CapturedStream extends EventEmitter {
	async start(cmd, {
		constraints, timeSlice, restreamInterval, bitrate, mimeType
	}) {
		this.constraints = constraints;
		this.timeSlice = timeSlice;
		this.restreamInterval = restreamInterval;
		this.bitrate = bitrate;
		this.mimeType = mimeType;
		this.recorders = [];
		this.recordInterval = null;

		this.stream = await navigator.mediaDevices[cmd](constraints);
		this.resume();
	}

	resume() {
		this.recorders.push(this._newMediaRecorder());

		this.recordInterval = setInterval(() => {
			// We're going to restream

			// 1. Start up another recorder
			this.recorders.push(this._newMediaRecorder(() => {
				// When the recording started, remove the 1st recorder
				const rec = this.recorders.shift();

				// Stop it
				rec.onGetRequest = () => {
					rec.ondataavailable = null;
					rec.stop();

					// And now we're only going to wait for events from the
					// new recorder
				};
				rec.requestData();
			}));
		}, this.restreamInterval * this.timeSlice);
	}

	pause() {
		for(const rec of this.recorders) {
			rec.ondataavailable = null;
			rec.onGetRequest = null;
			rec.stop();
		}
		this.recorders = [];
		clearInterval(this.recordInterval);
	}

	stop() {
		this.pause();
		for(const track of this.stream.getTracks()) {
			track.stop();
		}
	}


	_newMediaRecorder(onStart) {
		const rec = new MediaRecorder(this.stream, {
			mimeType: this.mimeType,
			bitsPerSecond: this.bitrate * 1000
		});
		let onStartCalled = false;
		let buf = [];

		rec.ondataavailable = e => {
			if(!onStartCalled && onStart) {
				onStartCalled = true;
				onStart();
			}

			// Blob to string
			const reader = new FileReader();
			reader.addEventListener("loadend", e => {
				const arrayBuffer = e.srcElement.result;
				buf = buf.concat(Array.from(new Uint8Array(arrayBuffer)));

				if(rec.onGetRequest) {
					rec.onGetRequest();

					const base64 = (new Buffer(buf)).toString("base64");
					this.emit("capture", {
						data: `z${base64}`,
						type: rec.mimeType
					});
				}
			});
			reader.readAsArrayBuffer(e.data);
		};

		rec.start(this.timeSlice);

		return rec;
	}
}