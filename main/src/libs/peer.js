import {zeroPage} from "@/zero";

export const handlePeerReceive = ({message, hash, ip}) => {
	try {
		if(message.startsWith("chat|")) {
			// ok
		} else {
			let [videoId, sourceId, type, packetId, duration, data] = message.split(/[*@?!,]/);

			if(videoId.length > 30) {
				throw new Error("Too long video ID");
			} else if(sourceId !== "camera" && sourceId !== "screen") {
				throw new Error("No data");
			} else if(data === "pause") {
				// ok
			} else if(type.length > 30) {
				throw new Error("Too long mimetype");
			} else if(!packetId.split("").every(char => !isNaN(parseInt(char, 10)))) {
				throw new Error("Invalid packet ID");
			} else if(isNaN(parseFloat(duration))) {
				throw new Error("Invalid duration");
			} else if(!data) {
				throw new Error("No data");
			}

			// console.log(`Validating message '${hash}' from ${ip}`);
			zeroPage.cmd("peerValid", [hash]);
		}
	} catch(e) {
		console.warn(`Rejecting message '${hash}' from ${ip}. Reason: ${e.message}`);
		zeroPage.cmd("peerInvalid", [hash]);
	}
};