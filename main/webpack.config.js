const path = require("path");
const webpack = require("webpack");
const UglifyJSPlugin = require("uglifyjs-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const ZipFilesPlugin = require("webpack-zip-files-plugin");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;

const common = require("./webpack.common.js");

let externChunks = require("./extern-chunks").map(obj => {
	const chunk = Object.keys(obj)[0];
	return {
		chunk,
		regexp: obj[chunk]
	};
});
let chunkPlugins = [];

for(let i = 0; i < externChunks.length; i++) {
	const name = (externChunks[i + 1] || {chunk: "common"}).chunk;
	const regexp = externChunks[i].regexp;
	chunkPlugins.push(
		new webpack.optimize.CommonsChunkPlugin({
			name,
			minChunks: module => !regexp.test(module.resource || "")
		})
	);
}

module.exports = [
	common(
		{
			[externChunks[0].chunk]: ["./main.js"]
		},
		[
			new HtmlWebpackPlugin({
				title: "StreamZ",
				template: "./index.html",
				seo: {
					keywords: "zeronet,realtime,communication",
					description: "StreamZ"
				}
			}),
			new CopyWebpackPlugin([
				{
					from: "./dbschema.json",
					to: "./dbschema.json"
				}
			]),
			new CopyWebpackPlugin([
				{
					from: "./content.json",
					to: "./content.json"
				}
			]),
			new CopyWebpackPlugin([
				{
					from: "./data",
					to: "./data"
				}
			]),
			new CopyWebpackPlugin([
				{
					from: "./hub",
					to: "./hub"
				}
			]),
			new CopyWebpackPlugin([
				{
					from: path.join(__dirname, "..", "logo.png"),
					to: "./favicon.png"
				}
			]),

			/*
			new ZipFilesPlugin({
				entries: [
					{
						src: path.join(__dirname, "src", "extension"),
						dist: "/"
					}
				],
				output: path.join(__dirname, "dist", "StreamZ"),
				format: "zip",
				ext: "xpi"
			}),
			*/

			new UglifyJSPlugin({
				cache: true,
				parallel: true
			}),

			new BundleAnalyzerPlugin({
				analyzerPort: 8275
			})
		].concat(chunkPlugins),
		{
			extern: false
		},
		undefined,
		path.resolve(__dirname, "./dist")
	)
];